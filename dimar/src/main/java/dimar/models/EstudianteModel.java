package dimar.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "estudiante")
public class EstudianteModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    private Integer idpersona;
    private String carrera;
    private String codestudiantil;
    private String anoingreso;

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdPersona() {
        return idpersona;
    }

    public void setIdPersona(Integer idpersona) {
        this.idpersona = idpersona;
    }
    
    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
    public String getCodEstudiantil() {
        return codestudiantil;
    }

    public void setCodEstudiantil(String codestudiantil) {
        this.codestudiantil = codestudiantil;
    }
    public String getAnoIngreso() {
        return anoingreso;
    }

    public void setAnoIngreso(String anoingreso) {
        this.anoingreso = anoingreso;
    }

}
