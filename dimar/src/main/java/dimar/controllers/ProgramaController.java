package dimar.controllers;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dimar.models.ProgramaModel;
import dimar.services.ProgramaService;


@RestController
@RequestMapping("/programas")
public class ProgramaController {
    @Autowired
    ProgramaService programaService;

    /**
     * @return
     */
    @GetMapping()
    public ArrayList<ProgramaModel> obtenerProgramas(){
        return programaService.obtenerPrograma();
    }

    @PostMapping()
    public ProgramaModel guardarPrograma(@RequestBody ProgramaModel programas){
        return this.programaService.guardarPrograma(programas);
    }

    @GetMapping( path = "/{id}")
    public Optional<ProgramaModel> obtenerProgramaPorId(@PathVariable("id") Long id) {
        return this.programaService.obtenerPorId(id);
    }

  
    @DeleteMapping( path = "/{id}")
    public String eliminarPorId(@PathVariable("id") Long id){
        boolean ok = this.programaService.eliminarPrograma(id);
        if (ok){
            return "Se eliminó la persona con id " + id;
        }else{
            return "No pudo eliminar la persona con id" + id;
        }
    }
    
}
