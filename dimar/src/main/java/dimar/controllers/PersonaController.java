package dimar.controllers;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dimar.models.PersonaModel;
import dimar.services.PersonaService;


@RestController
@RequestMapping("/persona")
public class PersonaController  {
    @Autowired
    PersonaService personaService;

    /**
     * @return
     */
    @GetMapping()
    public ArrayList<PersonaModel> obtenerPersonas(){
        return personaService.obtenerPersonas();
    }

    @PostMapping()
    public PersonaModel guardarPersona(@RequestBody PersonaModel persona){
        return this.personaService.guardarPersona(persona);
    }

    @GetMapping( path = "/{id}")
    public Optional<PersonaModel> obtenerPersonaPorId(@PathVariable("id") BigInteger id) {
        return this.personaService.obtenerPorId(id);
    }

    @DeleteMapping( path = "/{id}")
    public String eliminarPorId(@PathVariable("id") BigInteger id){
        boolean ok = this.personaService.eliminarPersona(id);
        if (ok){
            return "Se eliminó la persona con id " + id;
        }else{
            return "No pudo eliminar la persona con id" + id;
        }
    }

}