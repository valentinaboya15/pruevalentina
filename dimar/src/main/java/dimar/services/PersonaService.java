
package dimar.services;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dimar.models.PersonaModel;
import dimar.repositories.PersonaRepository;

@Service
public class PersonaService {
    @Autowired
    PersonaRepository personaRepository;
    
    public ArrayList<PersonaModel> obtenerPersonas(){
        return (ArrayList<PersonaModel>) personaRepository.findAll();
    }

    @SuppressWarnings("null")
    public PersonaModel guardarPersona(PersonaModel persona){
        return personaRepository.save(persona);
    }

    @SuppressWarnings("null")
    public Optional<PersonaModel> obtenerPorId(BigInteger id){
        return personaRepository.findById(id);
    }


    @SuppressWarnings("null")
    public boolean eliminarPersona(BigInteger id) {
        try{
            personaRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }


    
}