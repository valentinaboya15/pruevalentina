package dimar.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dimar.models.EstudianteModel;
import dimar.repositories.EstudianteRepository;

@Service
public class EstudianteService {
    @Autowired
    EstudianteRepository estudianteRepository;

    public ArrayList<EstudianteModel> obtenerEstudiantes(){
        return (ArrayList<EstudianteModel>) estudianteRepository.findAll();
    }

    @SuppressWarnings("null")
    public EstudianteModel guardarEstudiante(EstudianteModel estudiante){
        return estudianteRepository.save(estudiante);
    }

       @SuppressWarnings("null")
    public Optional<EstudianteModel> obtenerPorId(Long id){
        return estudianteRepository.findById(id);
    }


    @SuppressWarnings("null")
    public boolean eliminarEstudiante(Long id) {
        try{
            estudianteRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }
    
}
