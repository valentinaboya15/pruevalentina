package dimar.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dimar.models.ProgramaModel;
import dimar.repositories.ProgramaRepository;

@Service
public class ProgramaService {
    @Autowired
    ProgramaRepository programaRepository;
    
    public ArrayList<ProgramaModel> obtenerPrograma(){
        return (ArrayList<ProgramaModel>) programaRepository.findAll();
    }

    @SuppressWarnings("null")
    public ProgramaModel guardarPrograma(ProgramaModel programas){
        return programaRepository.save(programas);
    }

    @SuppressWarnings("null")
    public Optional<ProgramaModel> obtenerPorId(Long id){
        return programaRepository.findById(id);
    }

    @SuppressWarnings("null")
    public boolean eliminarPrograma(Long id) {
        try{
            programaRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }
    
}
