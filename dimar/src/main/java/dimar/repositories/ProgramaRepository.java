package dimar.repositories;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dimar.models.ProgramaModel;

@Repository

public interface ProgramaRepository extends CrudRepository<ProgramaModel, Long> {
    /**
     * @param id
     * @return
     */
    public abstract ArrayList<ProgramaModel> findById(Integer id);

}