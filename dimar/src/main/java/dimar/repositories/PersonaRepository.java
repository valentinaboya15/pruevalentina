
package dimar.repositories;

import java.math.BigInteger;
import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dimar.models.PersonaModel;

@Repository
public interface PersonaRepository extends CrudRepository<PersonaModel, BigInteger> {
    /**
     * @param id
     * @return
     */
    public abstract ArrayList<PersonaModel> findById(Integer id);

}