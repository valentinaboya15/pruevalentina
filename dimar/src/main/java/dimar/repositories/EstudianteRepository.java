package dimar.repositories;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dimar.models.EstudianteModel;

@Repository
public interface EstudianteRepository extends CrudRepository<EstudianteModel, Long> {
    /**
     * @param id
     * @return
     */
    public abstract ArrayList<EstudianteModel> findById(Integer id);

}




